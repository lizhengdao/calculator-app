package com.abdulvahap.calculatorapp;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    private EditText result;                //create a result instance in EditText class
    private EditText newNumber;             //create a newNumber instance in EditText class
    private TextView displayOperation;      //create a displayOperation instance in TextView class

    // Variables to hold the operands and type of calculations

    private Double operand1 = null;         //create a operand1 instance in Double class, assign null at first
    private String pendingOperation = "";  //create a pendingOperation instance in String class, assign "" at first

    private static final String STATE_PENDING_OPERATION = "PendingOperation";
    private static final String STATE_OPERAND1 = "Operand1";

    Button buttonDivide;
    Button buttonMultiply;
    Button buttonMinus;
    Button buttonPlus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonDivide = (Button) findViewById(R.id.buttonDivide);         // match buttonDivide instance with the buttonDivide Button widget
        buttonMultiply = (Button) findViewById(R.id.buttonMultiply);     // match buttonMultiply instance with the buttonMultiply Button widget
        buttonMinus = (Button) findViewById(R.id.buttonMinus);           // match buttonMinus instance with the buttonMinus Button widget
        buttonPlus = (Button) findViewById(R.id.buttonPlus);             // match buttonPlus instance with the buttonPlus Button widget


        //result = (EditText) findViewById(R.id.result);              // match result instance with the result EditText widget
        newNumber = (EditText) findViewById(R.id.newNumber);        // match newNumber instance with the newNumber EditText widget
        displayOperation = (TextView) findViewById(R.id.operation); // match displayOperation instance with the operation TextView widget

        Button button0 = (Button) findViewById(R.id.button0);       // match button0 instance with the button0 Button widget
        Button button1 = (Button) findViewById(R.id.button1);       // match button1 instance with the button1 Button widget
        Button button2 = (Button) findViewById(R.id.button2);       // match button2 instance with the button2 Button widget
        Button button3 = (Button) findViewById(R.id.button3);       // match button3 instance with the button3 Button widget
        Button button4 = (Button) findViewById(R.id.button4);       // match button4 instance with the button4 Button widget
        Button button5 = (Button) findViewById(R.id.button5);       // match button5 instance with the button5 Button widget
        Button button6 = (Button) findViewById(R.id.button6);       // match button6 instance with the button6 Button widget
        Button button7 = (Button) findViewById(R.id.button7);       // match button7 instance with the button7 Button widget
        Button button8 = (Button) findViewById(R.id.button8);       // match button8 instance with the button8 Button widget
        Button button9 = (Button) findViewById(R.id.button9);       // match button9 instance with the button9 Button widget
        Button buttonDot = (Button) findViewById(R.id.buttonDot);   // match buttonDot instance with the buttonDot Button widget

        Button buttonEquals = (Button) findViewById(R.id.buttonEquals);         // match buttonEquals instance with the buttonEquals Button widget
        final Button buttonDivide = (Button) findViewById(R.id.buttonDivide);         // match buttonDivide instance with the buttonDivide Button widget
        final Button buttonMultiply = (Button) findViewById(R.id.buttonMultiply);     // match buttonMultiply instance with the buttonMultiply Button widget
        final Button buttonMinus = (Button) findViewById(R.id.buttonMinus);           // match buttonMinus instance with the buttonMinus Button widget
        final Button buttonPlus = (Button) findViewById(R.id.buttonPlus);             // match buttonPlus instance with the buttonPlus Button widget


        //buttonEquals.setBackgroundResource(android.R.drawable.btn_default);
        buttonDivide.setBackgroundResource(android.R.drawable.btn_default);
        buttonMultiply.setBackgroundResource(android.R.drawable.btn_default);
        buttonMinus.setBackgroundResource(android.R.drawable.btn_default);
        buttonPlus.setBackgroundResource(android.R.drawable.btn_default);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {               //create a listener instance in View.OnClickListener class

                Button b = (Button) view;                         //create b instance in Button class to get a newNumber for calculation
                newNumber.append(b.getText().toString());         //get text of clicked button, convert to string and append to newNumber instance
                if (pendingOperation.equals("=")) {
                    newNumber.setText("");
                    newNumber.append(b.getText().toString());
                }
            }
        };
        button0.setOnClickListener(listener);                 //set OnClickListener to button0
        button1.setOnClickListener(listener);                 //set OnClickListener to button1
        button2.setOnClickListener(listener);                 //set OnClickListener to button2
        button3.setOnClickListener(listener);                 //set OnClickListener to button3
        button4.setOnClickListener(listener);                 //set OnClickListener to button4
        button5.setOnClickListener(listener);                 //set OnClickListener to button5
        button6.setOnClickListener(listener);                 //set OnClickListener to button6
        button7.setOnClickListener(listener);                 //set OnClickListener to button7
        button8.setOnClickListener(listener);                 //set OnClickListener to button8
        button9.setOnClickListener(listener);                 //set OnClickListener to button9
        buttonDot.setOnClickListener(listener);                 //set OnClickListener to buttonDot

        View.OnClickListener opListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {            //create a opListener instance in View.OnClickListener class
                Button b = (Button) view;                       //create b instance in Button class to get desired operation for calculation
                String op = b.getText().toString();             //get text of clicked button, convert to string assignn to op instance
                String value = newNumber.getText().toString();  //get text of newNumber, convert to string and assign to value instance
                try {
                    Double doubleValue = Double.valueOf(value);     //convert value instance string to double
                    performOperation(doubleValue, op);              //call perfromOperation method

                } catch (NumberFormatException e) {  //if user clicks dot button then try to operate, it is impossible to convert dot to double
                    newNumber.setText("");           // so catch the NumberFormatException
                }
                pendingOperation = op;                              //get the content of op to display
                displayOperation.setText(pendingOperation);         //set content of pendingOperation to displayOperation
                if (pendingOperation.equals("+") || pendingOperation.equals("-") || pendingOperation.equals("*") || pendingOperation.equals("/"))
                    setPressed(b.getId());
                try {
                    if (op.equals("=")) {
                        newNumber.setText(operand1.toString());
                        operand1 = null;
                        buttonDivide.setBackgroundResource(android.R.drawable.btn_default);
                        buttonMultiply.setBackgroundResource(android.R.drawable.btn_default);
                        buttonMinus.setBackgroundResource(android.R.drawable.btn_default);
                        buttonPlus.setBackgroundResource(android.R.drawable.btn_default);
                    }
                } catch (NullPointerException e) {
                }
            }
        };
        buttonDivide.setOnClickListener(opListener);                //set OnClickListener to buttonDivide
        buttonMultiply.setOnClickListener(opListener);              //set OnClickListener to buttonMultiply
        buttonMinus.setOnClickListener(opListener);                 //set OnClickListener to buttonMinus
        buttonPlus.setOnClickListener(opListener);                  //set OnClickListener to buttonPlus
        buttonEquals.setOnClickListener(opListener);                //set OnClickListener to buttonEquals

        Button buttonNeg = (Button) findViewById(R.id.buttonNeg);   //create a buttonNeg instance and match to buttonNeg Button widget

        buttonNeg.setOnClickListener(new View.OnClickListener() {   //set OnClickListener to buttonNeg button
            @Override
            public void onClick(View view) {
                String value = newNumber.getText().toString();      // get value of newNumber instance, convert to string
                if (value.length() == 0) {                           // if it has zero lenght (nothing is typed)
                    newNumber.setText("-");                         // set - sign into newNumber instance
                } else {                                              // else
                    try {
                        Double doubleValue = Double.valueOf(value); //convert value instance string to double
                        doubleValue *= -1;                          //multiply doubleValue with -1 and assign it to doubleValue again
                        newNumber.setText(doubleValue.toString());  //convert doubleValue to string and set into newNumber instance
                    } catch (NumberFormatException e) { // if user clicks dot then try to negate, it is imposssible to negate
                        newNumber.setText("");    // so catch the NumberFormatException
                    }
                }

            }
        });


        Button buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operand1 = null;
                pendingOperation = "=";
                newNumber.setText("");
                //result.setText("");
                displayOperation.setText("");
                reset();

            }
        });

        Button buttonDelete = (Button) findViewById(R.id.buttonDel);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lastNumber = newNumber.getText().toString();
                if (lastNumber.length() > 0 && newNumber.getText().toString() != null) {
                    newNumber.setText(lastNumber.substring(0, lastNumber.length() - 1));
                }
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_PENDING_OPERATION, pendingOperation);//save the pending operation when screen is rotated(Activity Lifecycle)
        if (operand1 != null) {
            outState.putDouble(STATE_OPERAND1, operand1);             //save the operand when screen is rotated(Activity Lifecycle)
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        pendingOperation = savedInstanceState.getString(STATE_PENDING_OPERATION); //get back the pending operation after screen is rotated(Activity Lifecycle)
        operand1 = savedInstanceState.getDouble(STATE_OPERAND1);            //get back the operand after screen is rotated(Activity Lifecycle)
        displayOperation.setText(pendingOperation);                         //display current pendingOperation after rotation
    }

    private void performOperation(Double value, String operation) {         //create a performOperation method which uses a Double and a String instance
        if (null == operand1) {                                             //if operand is null (User haven't entered first number )
            operand1 = value;                                               //assign operand1 to value
        } else {                                                            //else (if user enters second number for calculation
            if (pendingOperation.equals("=")) {                             //if pendingOperation is =
                pendingOperation = operation;                               //assign pendingOperation to operation
            }
            switch (pendingOperation) {
                case "=":
                    operand1 = value;
                    break;
                case "/":
                    if (value == 0) {
                        operand1 = 0.0;
                    } else {
                        operand1 /= value;
                    }
                    break;
                case "*":
                    operand1 *= value;
                    break;
                case "-":
                    operand1 -= value;
                    break;
                case "+":
                    operand1 += value;
                    break;
            }
        }
        //result.setText(operand1.toString());
        newNumber.setText("");
    }

    int previousId = 0;

    private void setPressed(int id) {
        if (previousId != 0) {
            Button previouslyPressedButton = (Button) findViewById(previousId);
            previouslyPressedButton.setBackgroundResource(android.R.drawable.btn_default);
        }
        Button button = (Button) findViewById(id);
        button.setBackgroundColor(Color.RED);
        //button.setBackgroundResource(android.R.drawable.presence_online);
        previousId = id;
    }

    private void reset() {

        buttonDivide.setBackgroundResource(android.R.drawable.btn_default);
        buttonMultiply.setBackgroundResource(android.R.drawable.btn_default);
        buttonMinus.setBackgroundResource(android.R.drawable.btn_default);
        buttonPlus.setBackgroundResource(android.R.drawable.btn_default);


    }
}